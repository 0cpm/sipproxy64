#!/bin/bash
#
# Iterate over the tests, passing each in turn to SIPproxy64.
#
# Tests are stored in individual directories, storing:
#  - ARGS holds commandline arguments (excluding -t)
#  - send-N-IP-PORT holds the Nth packet to send over UDP to IP:PORT
#  - EXPECT contains the expected output from the test
#  - ACTUAL contains the observed output from the test
#  - DIFF contains the difference between expected and observed output
#
# The script may be called with arguments; when available, they override
# defaults; they must be provided in this order:
#  1. the path to an executable to test (instead of ../sipproxy64)
#  2. the test to execute (instead of * for all)
#  3. action flags, combined to a string instead of doing it all:
#	- D=start daemon
#	- T=test mode for the deamon
#	- S=send test packets
#	- C=print comparison result
#	- K=kill the SIPproxy64 daemons started from this terminal
#
# From: Rick van Rein <rick@openfortress.nl>

EXIT=0

set -o pipefail

if [ $# -ge 1 ]
then
	SIPPROXY="$1"
else
	SIPPROXY=$(which sipproxy64)
fi

if [ $# -ge 2 ]
then
	TDIR="$2"
else
	TDIR=`dirname "$0"`
fi

if [ $# -ge 3 ]
then
	TESTPATN="$3"
else
	unset TESTPATN
fi

if [ $# -ge 4 ]
then
	ACTION_DMN=`echo $4 | grep D | wc -l`
	ACTION_TMO=`echo $4 | grep T | wc -l`
	ACTION_SND=`echo $4 | grep S | wc -l`
	ACTION_CMP=`echo $4 | grep C | wc -l`
	ACTION_KIL=`echo $4 | grep K | wc -l`
	ACTION_PRN=0
else
	ACTION_DMN=1
	ACTION_TMO=1
	ACTION_SND=1
	ACTION_CMP=1
	ACTION_KIL=1
	ACTION_PRN=1
fi

for test in $TDIR/${TESTPATN:-*}
do
	if [ -d "$test" ]
	then
		[ $ACTION_PRN -eq 1 ] && echo >&2 -n "Testing $test... "
		ARGS=`cat "$test/ARGS"`
		TARG=''
		[ $ACTION_TMO -eq 1 ] && TARG=$(ls "$test"/send-* | sed 's/^.*$/-t/')
		EXPECT="$test/EXPECT"
		OUTDIR=$(pwd)/t/$(basename "$test")
		mkdir -p $OUTDIR
		ACTUAL="$OUTDIR/ACTUAL"
		DIFF="$OUTDIR/DIFF"
		cp /dev/null "$ACTUAL"
		cp /dev/null "$DIFF"
		if [ $ACTION_DMN -eq 1 ]
		then
			echo "$SIPPROXY" $TARG $ARGS
			( "$SIPPROXY" $TARG $ARGS > "$ACTUAL" ; echo >> "$ACTUAL" ; echo >> "$ACTUAL" "Exit code $?" ) &
		fi
		if [ $ACTION_SND -eq 1 ]
		then
			for send in "$test"/send-*
			do
				NCARGS=`echo $send | sed -e 's/.*send-[0-9a-z]*-//' -e 's/-/ /g'`
				#DEBUG# echo Sending $send through nc -w 0 -u -p 50607 $NCARGS
				cat "$send" | sed -e '/^#.*$/d' -e 's///' -e 's/$//' | nc -w 0 -u -p 50607 $NCARGS
				sleep 0.3
				#DEBUG# echo Done sending
			done
			sleep 0.9
		fi
		[ $ACTION_KIL -eq 1 ] && sleep 1
		PID=`ps -T -C sipproxy64 --no-headers -opid`
		if [ -n "$PID" ]
		then
			#DEBUG# echo kill -HUP $PID
			[ $ACTION_KIL -eq 0 ] && echo Daemon running with pid $PID
			[ $ACTION_KIL -eq 1 ] && kill -HUP $PID
			[ $ACTION_KIL -eq 1 ] && echo >&2 Had to hangup $PID for test $test
			[ $ACTION_KIL -eq 1 ] && EXIT=1
		elif  [ `diff -q "$EXPECT" "$ACTUAL" > /dev/null ; echo $?` -ne 0 ]
		then
			[ $ACTION_CMP -eq 1 ] && diff -U 1000 "$EXPECT" "$ACTUAL" > "$DIFF"
			[ $ACTION_CMP -eq 1 ] && echo >&2 Test failed, see $DIFF
			[ $ACTION_CMP -eq 1 ] && EXIT=1
		else
			[ $ACTION_CMP -eq 1 ] && rm "$DIFF"
			[ $ACTION_CMP -eq 1 ] && echo >&2 OK
		fi
	fi
done

exit $EXIT
