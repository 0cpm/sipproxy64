/* Text portion handling
 *
 * This file is part of 0cpm Firmerware and SIPproxy64.
 * Its origin (and the place to patch) is in the 0cpm Firmerware.
 *
 * 0cpm Firmerware is Copyright (c)2011 Rick van Rein, OpenFortress.
 *
 * 0cpm Firmerware is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * 0cpm Firmerware is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 0cpm Firmerware.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef HEADER_TEXT
#define HEADER_TEXT

#include <stdio.h>

/* Text is handled as series of characters that may be contained in
 * a larger string.  So, they are not necessarily terminated with an
 * ASCII NUL character, as is common in C.  This makes it possible
 * to "point" into a SIP message without editing it.
 */
typedef struct text_pointer textptr_t;
struct text_pointer {
	char *str;
	uint16_t len;
};


/* See if two (textptr_t *) point to the same string */
static inline bool texteq (textptr_t const *a, textptr_t const *b) {
	if (a->len != b->len) {
		return false;
	}
	return memcmp (a->str, b->str, a->len) == 0;
}

/* See if a (textptr_t *) points to a NULL string */
static inline bool textisnull (textptr_t const *a) {
	return (a->str == NULL);
}

/* Set a (textptr_t *) to a NULL string */
static inline void textnullify (textptr_t *a) {
	a->str = NULL;
}

/* Append a textptr_t to a plain C string */
static inline char *txtcat (char *pout, textptr_t const *data) {
	memcpy (pout, data->str, data->len);
	return pout + data->len;
}

/* Attach an integer, possibly in hex, to a C string */
static inline char *intcat (char *pout, uint32_t value) {
	return pout + sprintf (pout, "%d", value);
}
static inline char *hexcat (char *pout, uint32_t value, uint8_t digits) {
	char fmtstr [20];
	sprintf (fmtstr, "%%%dx", digits);
	return pout + sprintf (pout, fmtstr, value);
}

#endif
