# Build structures for SIPproxy64
#
# From: Rick van Rein <rick@openfortress.nl>

cmake_minimum_required (VERSION 3.18 FATAL_ERROR)
project ("SIPproxy64" VERSION 0.11 LANGUAGES C)

include (GNUInstallDirs)

option (DEBUG
    "Switch on output and flags that aid developpers in debugging"
    OFF)

if (DEBUG)
    add_compile_options (-DDEBUG -DLOG_STYLE=LOG_STYLE_STDERR -ggdb3 -O0)
endif()

add_executable (sipproxy64
	src/proxy.c
	src/parse.c
)

target_include_directories (sipproxy64
	PRIVATE ${CMAKE_SOURCE_DIR}/include
)

install (TARGETS sipproxy64
	RUNTIME DESTINATION ${CMAKE_INSTALL_SBINDIR}
)

install (FILES sipproxy64.man
	DESTINATION ${CMAKE_INSTALL_MANDIR}/man8/
	RENAME sipproxy64.8
)

install (DIRECTORY etc/sipproxy64
	DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}
)

install (FILES etc/init.d/sipproxy64
	DESTINATION ${CMAKE_INSTALL_SYSCONFDIR}/init.d
)

enable_testing ()

foreach (_TEST IN ITEMS demo crash rfc3261)
	add_test (
		NAME t/${_TEST}
		COMMAND ${CMAKE_SOURCE_DIR}/t/runall.sh
			# Binary under test
			${CMAKE_BINARY_DIR}/sipproxy64
			# Test source directory (output relative to working dir)
			${CMAKE_SOURCE_DIR}/t
			# Test to run
			${_TEST}
		WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
	)
endforeach ()

