.TH SIPPROXY64 8 "April 30, 2022"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
sipproxy64 \- A SIP/RTP proxy to crossover between IPv4 and IPv6
.SH SYNOPSIS
.B sipproxy64
\fB\-l\fR \fIv4local\fR \fB\-L\fR \fIv6local\fR [\fB\-u\fR \fIv4uplink\fR] [\fB\-U\fR \fIv6uplink\fR] [\fB-w\fR] [\fIv4phone\fR=\fIv6phone\fR...]
.SH DESCRIPTION
.PP
If IPv4 and IPv6 are different universes or different dimensions, then by that
metaphor \fBSIPproxy64\fR is a wormhole between them.  In order to make a SIP phone
in the other universe reachable, it listens to a local IP address that serves
as a proxy in this universe to get to the phone in the other.  In the other universe,
it sends from a local IP address in that universe, to which it listens for
responses to be warped back to this universe.
.PP
More technically, \fBSIPproxy64\fR is a proxy for SIP and RTP traffic between IPv6 and IPv4.
This proxy does nothing clever, it merely exchanges IPv4 and IPv6 addresses and passes
on the traffic to the other network.  As a concrete limitation, it won't do DNS.  If anything
needs to be resolved, even DNS lookups, the traffic should be directed to a proxy for
further handling.  This is just a transitionary tool while IPv4 is phasing out and
IPv6 is phasing in, so \fBSIPproxy64\fR is not the suitable place for functions like
wiretapping, that may be provided for in other proxies.
.PP
\fBSIPproxy64\fR has its own simple \fBRTPproxy64\fR built in.  This means that no external
application is required to tunnel traffic.  The added traffic for SIP usually
does no harm to an \fBRTPproxy64\fR, and getting it there may be done through any form
of SIP traffic shaping, if so desired, including even selective pickups of call
requests by the \fBSIPproxy64\fR hosts.  Note however, that 1 Gbps can carry as many
as 12500 calls at ISDN-quality, so this is not a limit to reach quickly.
.PP
The actual task of \fBSIPproxy64\fR is to find IPv6 addresses in messages entering its IPv6
side, and IPv4 address on messages entering its IPv4 side.  It then exchanges the address
from one address family with a matching address in the other.  To that end, it holds a
list of address pairs.  One address pair holds the \fBSIPproxy64\fR's sides.  Furthermore,
an address pair is constructed for every phone declaration.  Aside from the target URI,
the SIP headers that may be rewritten are Contact, From and To.  Non-numeric names
will be left as they are, assuming that DNS has entries for them in both address families.
.SH OPTIONS
.TP
\fB\-l\fR \fIv4local\fR
.TP
\fB\-\-v4local\fR=\fIv4local\fR
Local listen address for the proxy's IPv4 side.  This address must be specified.
It is possible to append a \fR:\fIport\fR, otherwise port 5060 will be assumed.
.TP
\fB\-L\fR \fIv6side\fR
.TP
\fB\-\-v6local\fR=\fIv6side\fR
Local listen address for the proxy's IPv6 side.  This address must be specified.
This must be a /64 address if the \fB\-u\fR option is to be of any use.
It is possible to append a \fR:\fIport\fR, otherwise port 5060 will be assumed.
Note that the IPv6 address notation overlaps with the attached port.  It is
a good idea to always enclose IPv6 addresses in square brackets to avoid
attempts to interpret the last part as a port number.
.TP
\fB\-u\fR \fIv4uplink\fR
.TP
\fB\-\-v4uplink\fR=\fIv4uplink\fR
Optional upstream proxy address for IPv4 traffic.  This address is optional.  Without it,
the only proxying option will be to use forwarding addresses from Via headers,
which must then be an IPv4 address.  This is normally set on return traffic, so
the only reason to specify this option is to be able to proxy new transactions
from IPv6 to IPv4.
.TP
\fB\-U\fR \fIv6uplink\fR
.TP
\fB\-\-v6uplink\fR=\fIv6uplink\fR
Optional upstream proxy address for IPv6 traffic.  This address is optional.  Without it,
the only proxying option will be to use forwarding addresses from Via headers,
which must then be an IPv6 address.  This is normally set on return traffic, so
the only reason to specify this option is to be able to proxy new transactions
from IPv4 to IPv6.
.TP
\fB\-w\fR
.TP
\fB\-\-wrapped\fR
Specify that SIPproxy64 is wrapped into a smart SIP layer that does things like access control and other clever things.  If this wrapper handles translation of the Contact header, it should not be replaced at all by \fBSIPproxyte\fR, which is the effect of this option.
.TP
\fB\-t\fR
.TP
\fB\-\-test\fR
Run in test mode.  This means that one message is received and dumped on the standard output; it is then mapped to an output message, but instead of sending the output is also dumped on the standard output.  The SIPproxy64 daemon will terminate with exit code 0 after having handled as many test messages as there are test options, so it is meaningful to supply multiple test options.
.TP
\fIv4phone\fR=\fIv6phone\fR
Specify an IP address for a phone on both the IPv4 and IPv6 side of \fBSIPproxy64\fR.
One of these addresses will be bound locally, to implement a local proxy for
the other, remote address of the phone.  Most commonly, this will be used to
assign an IPv6 proxy address to an IPv4-only phone or PBX.  Note that the
local address must already exist.  Any number of these parameters can be
supplied, including even zero (in which case a simple proxy mode is assumed,
meaning that \fB\-u\fR and \fB\-U\fR are required).
.IP
The notation \fIv4addr\fR=\fIv6addr\fR is actually a bit more advanced
than just a pair of addresses; there can be \fR:\fIport\fR attachments
as well.  This may cause confusion in \fIv6addr\fR which can be avoided
by a sound habit of always enclosing IPv6 addresses in square brackets.
This is supported by the \fBSIPproxy64\fR grammar.
.IP
Abbreviated forms are also feasible for both \fIv4addr\fR and \fIv6addr\fR,
when the contain only the \fR:\fIport\fR notation.  In this case, the
address will be taken from the listening address on the respective side
of \fBSIPproxy64\fR.  This allows the program to be used with only a
single IPv6 address and a single IPv4 address.
.IP
If no port is supplied, a default port number is used.  For the first
\fIv4phone\fR=\fIv6phone\fR pair this will be 5062, for the next it
is 5064, and so on.  A special form \fR=\fR is permitted to skip the
default port number in a position; this may be helpful to avoid tedious
renumbering operations involving phone reprogramming when one is
removed.
.SH SIGNALS
.PP
It is possible to send a \fBHUP\fR signal to \fBSIPproxy64\fR, after
which it will unbind from its SIP ports.  This makes it possible for
a new instance of \fBSIPproxy64\fR to bind to those ports, or perhaps
to others if the configuration has changed.  Since the transport used
is UDP, no client should notice a quick changeover done this way.
To further smoothe the changeover, any \fBRTPproxy64\fR services in
progress will continue to keep the hung up \fBSIPproxy64\fR alive
until they terminate.  With SIP signaling not being processed anymore,
the only way for such termination of RTP traffic will be a delay of
60 seconds without data.  In short, sending \fBHUP\fR to \fBSIPproxy64\fR
will ask it to gently go away, and immediately make place for a new
incarnation of itself.
.SH EXAMPLES
.PP
A home router's SIP service may be exported over IPv6.  To be able
to actually call local phones, the service must accept incoming
calls from unregistered phones, and not demand authentication on
the invitation for the call either.
.PP
Assume the home router at 192.0.2.1, and the \fBSIPproxy64\fR to sit
between 192.0.2.12 and 2001:db8:187:4::192.0.2.12.  Also assume
an IPv6-side smarter proxy at 2001:db8:6::87.
.PP
sipproxy64 -l 192.0.2.21 -L 2001:db8:187:4::192.0.2.12 -u 192.0.2.1 -U 2001:db8:6::87
.PP
Another example discloses IPv4-only phones on the IPv6 network, each
at its own IP-address.  This scenario is chiefly important for a
small setup; larger numbers of phones usually register with a PBX
which, if it is IPv4-only, can be disclosed to IPv6 using the approach
above.
.PP
Assume the phones to be located at 10.0.0.138 and 10.0.0.139, with
intended IPv6 addresses 2001:db8:187:4::10:0:0:138 and
2001:db8:187:4::10:0:0:139.  The phones may or may not register through
the \fBSIPproxy64\fR; that all depends on the possibilities of the
registrar.  The following example assumes no smarter proxies to be
configured, but purely to route call traffic.  Also, it assumes to
run at 10.0.0.17 annex 2001:db8:187:4::10:0:0:17
.PP
sipproxy64 -l 10.0.0.17 -L 2001:db8:187:4::10:0:0:17 10.0.0.138=2001:db8:187:4::10:0:0:138 10.0.0.139=2001:db8:187:4::10:0:0:139
.PP
If the phone needs to call out over IPv6 instead of IPv4, direct the
SIP traffic through \fBSIPproxy64\fR.  One way of doing that is to specify
the IPv4-side of the proxy as an outgoing proxy.
.PP
If an IPv4-only phone needs to register with an IPv6 proxy, specify
that proxy with the \fB\-U\fR parameter.  In effect, \fBSIPproxy64\fR will
introduce an additional address mapping from the \fB\-l\fR address to the
\fB\-U\fR address.  Likewise, introducing \fB\-u\fR will cause an additional
address mapping between the \fB\-u\fR address and the \fB\-L\fR address.
.SH ALGORITHM
.PP
The \fBSIPproxy64\fR is a stateless SIP proxy.  This means that it is not
aware of connections that are live.  The built-in \fBRTPproxy64\fR does keep
state on connections, but it too will timeout if no traffic is passing
through it.  In other words, the system will always get back to an empty
initial state if left alone.
.PP
To achieve statelessness, SIP sends information in Route: headers
that tell a stateless proxy like ours where to forward traffic to.
\fBSIPproxy64\fR makes use of those headers as soon as a call has been setup.
.PP
The general idea of \fBSIPproxy64\fR is to listen to IPv6 addresses to
substitute for their lack on IPv4 phones.  It can do the opposite
as well, if the need ever arises.  In general, it maps between the
two address families IPv4 and IPv6 by listening on one address and
forwarding to the other.  It keeps an internal list of mappings
from one to the other.
.PP
If traffic arrives at an IP that is added by \fBSIPproxy64\fR, then it
will lookup the real address of the phone and forward there.  In
doing so, it uses its generic listen address (from the \fB\-l\fR or \fB\-L\fR
option) as a sender address.  The port numbers are forwarded
as-is; the From: To: and Contact: headers in the message are
changed.
.PP
If traffic arrives at the general listen addresses (from the \fB\-l\fR
and \fB\-L\fR options), then the first attempt is to look at Route:
headers (unless it is an INVITE, in which case they would not be
reliable), and follow those.  If this fails, but a generic
uplink address has been configured (with the \fB\-u\fR or \fB\-U\fR option)
on the other side, then that is used as the destination address.
If this also fails, the request is refused and an error message
TODO is sent back to the sender.  Port numbers are kept as they
were on the incoming request.
.PP
In the last procedure, the sender address in the forwarded SIP
message depends on the sender on the incoming message.  If the
message came from a phone whose IP in the other address family
is added by \fBSIPproxy64\fR, then that IP is selected as the sender
address for the forwarded SIP message; if not, the outgoing
address is the generic address.
.PP
Note that an INVITE from the phone, if it is steered through
\fBSIPproxy64\fR, will be sent to the generic uplink in the other
address family, using the address in that family as added by
\fBSIPproxy64\fR.  In other words, an INVITE to the World will be
sent as an INVITE in the other address family.
.SH BUGS
.PP
The current SIPproxy64 takes no stand on being an open relay between
IPv4 and IPv6 networks.  Future versions may change this, perhaps even
at the default level.
.PP
It is possible in theory to listen promiscuously to an interface, or
to use a mangling firewall, as a way to force traffic for a subnet
through the \fBSIPproxy64\fR.  This is not currently supported, which means
that all \fIv4addr\fR=\fIv6addr\fR address pairs specified for phones must
have exactly one binding; none would not allow traffic to be captured
and both would be confusing, if not plain silly.
.PP
No support exists for routing requests to the same address family.
Although this makes the function of \fBSIPproxy64\fR very clear, it could
be argued that an IPv4-only device would always forward to the same
IPv4 address, without knowing if it is actually asking for an IPv6
alternative.  Input on this matter is requested to decide whether it
should be handled in \fBSIPproxy64\fR or not.
.SH AUTHOR
Written by Rick van Rein for the 0cpm project.
.SH LICENSE
Released under GNU Public License version 3.
.SH "SEE ALSO"
http://0cpm.org/ \- The 0cpm project from a user's view.
.PP
http://devel.0cpm.org/sipproxy64/ \- The home of this package.
