#
# NOTE: This is a DEPRECATED Makefile -- use CMake
#       Its only reason of being around is because of the RFC list commparison
#

INCDIR=include
TODOTMPINCDIR=../firmerware/include
CFLAGS=-ggdb3
# LDFLAGS=-pthread
LDFLAGS=
OBJS=proxy.o parse.o

all: sipproxy64 sipproxy64.8.gz #TODO# not-implemented

sipproxy64: $(OBJS)
	gcc $(LDFLAGS) -o $@ $(OBJS)

proxy.o: src/proxy.c Makefile
	gcc -c $(CFLAGS) -I $(INCDIR) -I $(TODOTMPINCDIR) -o $@ $<

parse.o: src/parse.c Makefile
	gcc -c $(CFLAGS) -I $(INCDIR) -I $(TODOTMPINCDIR) -o $@ $<

sipproxy64.8.gz: sipproxy64.man
	gzip --best < $< > $@

tags: src/proxy.c src/parse.c
	ctags --recurse src/proxy.c src/parse.c

test: sipproxy64 t/runall.sh
	t/runall.sh ./sipproxy64

install: sipproxy64 sipproxy64.8.gz
	install --strip --mode 755 sipproxy64 $(DESTDIR)/usr/sbin
	install         --mode 644 sipproxy64.8.gz $(DESTDIR)/usr/share/man/man8/sipproxy64.8.gz
	install         --mode 644 etc/sipproxy64/* $(DESTDIR)/etc/sipproxy64/

not-implemented: headerrfclist headerrfclist.implemented
	@diff -q headerrfclist headerrfclist.implemented > /dev/null || echo -n 'RFCs with headers not yet mapped: '
	@diff -q headerrfclist headerrfclist.implemented > /dev/null || @echo `diff headerrfclist headerrfclist.implemented | sed -e 1d -e '/^>.*/d' -e 's/^< //'`

headerrfclist: sip-headers
	sed < $< -e 's/^.*\[RFC//' -e 's/\].*//' | sort | uniq > $@

sip-headers: sip-parameters
	sed < $< -e '/Registry Name: Header Fields/,/Registry Name/!d' -e '/[ \t]\[RFC[0-9]*\]$$/!d' | sort | uniq > $@

sip-parameters:
	wget -q http://www.iana.org/assignments/sip-parameters

clean:
	rm -f test sipproxy64 $(OBJS) sipproxy64.8.gz sip-parameters headerrfclist

